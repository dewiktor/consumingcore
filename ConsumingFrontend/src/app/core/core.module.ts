import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { NavigationComponent } from './navigation/navigation.component';
import { FooterComponent } from './ui/footer/footer.component';
import { LayoutComponent } from './ui/layout/layout.component';
import { HeaderComponent } from './ui/header/header.component';

@NgModule({
  declarations: [NavigationComponent, FooterComponent, LayoutComponent, LayoutComponent, HeaderComponent],
  imports: [
    CommonModule,
    CoreRoutingModule
  ], exports: [
    LayoutComponent
  ]
})
export class CoreModule { }
