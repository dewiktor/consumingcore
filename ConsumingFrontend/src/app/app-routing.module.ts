import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'users',
    loadChildren: './users/users.module#UsersModule'
  },
  {
    path:'admin',
    loadChildren: './admin/admin.module#AdminModule'
  },
  {
    path: 'notes',
    loadChildren: './notes/notes.module#NotesModule'
  },
  {
    path: 'settings',
    loadChildren: './setting/setting.module#SettingModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
