import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  changePasswordForm = new FormGroup({
    newPassword: new FormControl(''),
    confirmPassword: new FormControl(''),
    oldPassword: new FormControl(''),
  });
  accountEmail = '';
  constructor() { }

  ngOnInit() {
  }

  changeAccountEmail() {
    alert(this.accountEmail);
  }

}
